#include <Vmc/Vmc.hpp>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
struct SolverTestPerf : Test
{
    Solver solver;
};

TEST_F(SolverTestPerf, shouldLoad)
{
    ASSERT_TRUE(solver.solve());
}

}