
$(shell touch .config)
read-config-parameter = $(shell cat .config | grep $(1) | sed 's/.*=//')

# $(call add-preprocessor-flag, parameter-name, flag-name)
define add-preprocessor-flag
ifeq ($(call read-config-parameter,$1),1)
CPPFLAGS += -D$2
endif
endef

# $(call add-compiler-cpp-standard, standard-name)
define add-compiler-cpp-standard
ifneq ($(call read-config-parameter,$1),)
CXXFLAGS += -std=$(call read-config-parameter,$1)
endif
endef

# $(call add-compiler-option, parameter-name)
define add-compiler-option
CXXFLAGS += $(call read-config-parameter,$1)
endef

# $(call set-legacy-cpp-compiler, flag-name)
define set-legacy-compiler
ifeq ($(call read-config-parameter,$1),1)
CXX = /usr/bin/g++-4.8
endif
endef

$(eval $(call add-preprocessor-flag,log-file-on,_LOG_FILE_ON))
$(eval $(call add-preprocessor-flag,log-trace-removed,_LOG_TRACE_REMOVED))
$(eval $(call add-compiler-cpp-standard,cpp-standard-version))
$(eval $(call add-compiler-option,optimization-level))
$(eval $(call set-legacy-compiler,legacy-compiler))
