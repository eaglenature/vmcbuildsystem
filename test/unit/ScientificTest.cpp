#include <Vmc/Scientific/Scientific.hpp>
#include <gtest/gtest.h>

namespace Vmc
{
namespace Scientific
{
struct ScientificTest : public ::testing::Test
{
};

TEST_F(ScientificTest, verifyGslFunction)
{
    ASSERT_DOUBLE_EQ(1.23, allocGslVectorAndReturnFirstElement());
}
}
}