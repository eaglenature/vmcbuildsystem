#include <Vmc/Scientific/Scientific.hpp>
#include "PrivateStuff.hpp"
#include <gsl/gsl_vector.h>

namespace Vmc
{
namespace Scientific
{
double allocGslVectorAndReturnFirstElement()
{
    gsl_vector* v = gsl_vector_alloc(3);
    for (auto i = 0; i < 3; ++i)
    {
        gsl_vector_set(v, i, 1.23 + i);
    }
    double first = gsl_vector_get(v, 0);
    gsl_vector_free(v);
    return isCustomImplementation(first) ? first : 3.14;
}
}
}