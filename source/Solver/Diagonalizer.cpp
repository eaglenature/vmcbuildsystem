#include <iostream>
#include <Vmc/Solver/Diagonalizer.hpp>

namespace Vmc
{
Diagonalizer::Diagonalizer(std::string name)
{
    std::cout << "Diagonalize: " << name << std::endl;
}

unsigned int Diagonalizer::getValue() const
{
    return 10;
}
}