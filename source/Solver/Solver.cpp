#include <iostream>
#include <Vmc/Solver/Solver.hpp>

namespace Vmc
{
Solver::Solver()
{
    std::cout << "Solver::Solver()" << std::endl;
}

bool Solver::solve() const
{
    std::cout << "Solver::solve()" << std::endl;
    return true;
}
}
