local_directory := test/unit
local_sources   := $(wildcard $(local_directory)/*.cpp)
local_libraries := $(libraries)
local_testlib   := -lgtest -lgtest_main -lgmock

$(eval $(call make-test, $(local_directory)/runUnitTestSuite, $(local_sources), $(local_libraries), $(local_testlib)))

