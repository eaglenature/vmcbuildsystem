
# $(call source-to-object, source-file-list)
source-to-object = $(subst .cpp,.o,$(filter %.cpp,$1))

# $(call make-library, library-name, source-file-list)
define make-library
  libraries += $1
  sources   += $2

  $1: $(call source-to-object,$2)
	@echo Creating library: $$@
	@$(AR) $(ARFLAGS) $$@ $$^
	@echo ======================================================================
endef

# $(call make-example, program-name, source-file-list, libraries-list)
define make-example
  examples += $1
  sources  += $2

  $1: $(call source-to-object,$2) $3
	@echo Creating example: $$@
	$(CXX)  -o $$@ $$^ $(LDFLAGS) $(LDLIBS)
	@echo ======================================================================
endef

# $(call make-test, program-name, source-file-list, libraries-list, test-library)
define make-test
  tests    += $1
  sources  += $2

  $1: $(call source-to-object,$2) $3
	@echo Creating test: $$@
	$(CXX) -o $$@ $$^ $4 $(LDFLAGS) $(LDLIBS)
	@echo ======================================================================
endef
