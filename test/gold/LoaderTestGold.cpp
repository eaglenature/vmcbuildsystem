#include <Vmc/Vmc.hpp>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
struct LoaderTestGold : Test
{
    Loader loader;
};

TEST_F(LoaderTestGold, shouldLoad)
{
    ASSERT_TRUE(loader.load());
}

}