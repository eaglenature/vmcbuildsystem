local_directory := example/TwistedGraphene
local_sources   := $(wildcard $(local_directory)/*.cpp)
local_libraries := $(libraries)

$(eval $(call make-example, $(local_directory)/TwistedGraphene, $(local_sources), $(local_libraries)))
