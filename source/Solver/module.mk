local_directory := source/Solver
local_sources   := $(wildcard $(local_directory)/*.cpp)

$(eval $(call make-library, $(local_directory)/libsolver.a, $(local_sources)))
