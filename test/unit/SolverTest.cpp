#include <Vmc/Vmc.hpp>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
struct SolverTest : Test
{
    Solver solver;
};

TEST_F(SolverTest, shouldLoad)
{
    ASSERT_TRUE(solver.solve());
}

}