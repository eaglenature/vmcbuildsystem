#include <iostream>
#include <Vmc/Vmc.hpp>
#include <gsl/gsl_vector.h>

int main()
{
    using namespace Vmc;

    std::cout << "Run TwistedGraphene application" << std::endl;
    Loader l;
    Solver s;

    gsl_vector* v = gsl_vector_alloc(3);
    for (auto i = 0; i < 3; ++i)
    {
        gsl_vector_set(v, i, 1.23 + i);
    }
    double first = gsl_vector_get(v, 0);
    gsl_vector_free(v);    

    std::cout << "Load and solve: " << loadAndSolve() << " " << first << std::endl;
}
