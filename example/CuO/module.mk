local_directory := example/CuO
local_sources   := $(wildcard $(local_directory)/*.cpp)
local_libraries := $(libraries)

$(eval $(call make-example, $(local_directory)/CuO, $(local_sources), $(local_libraries)))
