#pragma once
#include <string>

namespace Vmc
{

class Diagonalizer
{
public:
    Diagonalizer(std::string file);
    unsigned int getValue() const;
};
}