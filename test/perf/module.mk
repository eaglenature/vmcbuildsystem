local_directory := test/perf
local_sources   := $(wildcard $(local_directory)/*.cpp)
local_libraries := $(libraries)
local_testlib   := -lgtest -lgtest_main -lgmock -pthread

$(eval $(call make-test, $(local_directory)/runPerfTestSuite, $(local_sources), $(local_libraries), $(local_testlib)))
