#include <Vmc/Solver/Diagonalizer.hpp>
#include <string>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
class DiagonalizerTest : public Test
{
public:
    Diagonalizer diagonalizer{"brute force"};
};

TEST_F(DiagonalizerTest, shouldGetValue)
{
    ASSERT_EQ(10, diagonalizer.getValue());
}
}