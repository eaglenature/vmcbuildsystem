local_directory := source/Loader
local_sources   := $(wildcard $(local_directory)/*.cpp)

$(eval $(call make-library, $(local_directory)/libloader.a, $(local_sources)))
