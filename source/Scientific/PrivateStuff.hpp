#pragma once

namespace Vmc
{
namespace Scientific
{
inline bool isCustomImplementation(double x)
{
    if (x > 10.0)
        return false;
    return true;
}
}
}
