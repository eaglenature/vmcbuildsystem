local_directory := test/gold
local_sources   := $(wildcard $(local_directory)/*.cpp)
local_libraries := $(libraries)
local_testlib   := -lgtest -lgtest_main -lgmock -pthread

$(eval $(call make-test, $(local_directory)/runGoldTestSuite, $(local_sources), $(local_libraries), $(local_testlib)))
