-------------------------------------------------------------------------------
Common class declaration layout: ./AspaceBspaceClassName.hpp
-------------------------------------------------------------------------------
#ifndef __CLASS_NAME_HPP__
#define __CLASS_NAME_HPP__

namespace Aspace
{
namespace Bspace
{

constexpr unsigned int COMPILE_TIME_CONSTANT_VALUE = 10;

class Foo;

class ClassName : public ClassBase
{
public:                              // public before protected, protected before private 
    ClassName();                     // constructors before methods, methods before members
    ClassName(const ClassName&);
    ~ClassName();

    void publicMethod(const Foo& fooParam) const;

    PublicMember publicMember;       // or m_publicMember if class is big
    
protected:
    void protectedMethod();
    ProtectedMember protectedMember;

private:
    static unsigned s_staticVariable; // root of evil as shared between objects - should be well distinguishable

    void privateMethod();
    PrivateMember privateMember;
};

void freeFunctionUseClassName(const ClassName&);

} // namespace Bspace
} // namespace Aspace

#endif


-------------------------------------------------------------------------------
Common class definition layout:./AspaceBspaceClassName.cpp
-------------------------------------------------------------------------------
#include "AspaceBspaceFoo.hpp"
#include "AspaceBspaceClassName.hpp"

namespace Aspace
{
namespace Bspace
{

unsigned ClassName::s_staticVariable = 123;

ClassName::ClassName()
    : protectedMember()
    , privateMember()
{
}

void ClassName::publicMethod(const Foo& fooParam) const
{
    const unsigned localConstant = 0;
    unisigned numberOfSetCounters = localConstant;
    unisigned numberOfNotSetCounters = localConstant;

    const auto& counterList = fooParam.getCounterList();
    for (const auto& counter : counterList)
    {
        if (counter.isSet())
        {   
            ++numberOfSetCounters;
        }
        else
        {
            ++numberOfNotSetCounters;
        }
    }
}

void freeFunctionUseClassName(const ClassName& classInstance)
{
    while (true)
    {
        if (process(classInstance))
            break;
    }
}

} // namespace Bspace
} // namespace Aspace

-------------------------------------------------------------------------------
Abstract classes - interfaces (prefix I) ./IBar.hpp
-------------------------------------------------------------------------------

class IBar
{
public:
    virtual ~IBar() = default;
    virtual void publicMethod() const = 0;
};

class Bar : public IBar
{
public:
    Foo();
    void publicMethod() const override; // override or final
};
-------------------------------------------------------------------------------
