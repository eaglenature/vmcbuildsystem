#include <Vmc/Vmc.hpp>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
struct LoaderTest : Test
{
    Loader loader;
};

TEST_F(LoaderTest, shouldLoad)
{
    ASSERT_TRUE(loader.load());
}

}