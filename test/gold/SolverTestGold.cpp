#include <Vmc/Vmc.hpp>
#include <gtest/gtest.h>

using namespace ::testing;

namespace Vmc
{
struct SolverTestGold : Test
{
    Solver solver;
};

TEST_F(SolverTestGold, shouldLoad)
{
    ASSERT_TRUE(solver.solve());
}

}