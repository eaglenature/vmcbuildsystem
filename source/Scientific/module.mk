local_directory := source/Scientific
local_sources   := $(wildcard $(local_directory)/*.cpp)

$(eval $(call make-library, $(local_directory)/libscientific.a, $(local_sources)))
