#include <iostream>
#include <Vmc/Loader/Loader.hpp>

namespace Vmc
{
Loader::Loader()
{
    std::cout << "Loader::Loader" << std::endl;
    loadImpl();
}

bool Loader::load() const
{
    std::cout << "Loader::load" << std::endl;
    return true;
}
}
