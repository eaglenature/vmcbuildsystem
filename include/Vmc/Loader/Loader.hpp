#pragma once
#include <iostream>
#include <string>

namespace Vmc
{
class Loader
{
public:
    Loader();
    bool load() const;
private:
    void loadImpl() const
    {
        std::cout << "Loader::loadImpl" << std::endl;
    }
};

std::string loadAndSolve();
}