################################################################################################
# MAIN
################################################################################################
include script/build/definitions.mk
include script/build/configure.mk

################################################################################################
# Collect information from each module in these variables.
# Initialize them here as simple variables.
examples     :=
tests        :=
sources      :=
libraries    :=
include_dirs := source include /usr/local/include
external_libs:= -pthread -lgsl -lgslcblas -lm

objects       = $(subst .cpp,.o,$(sources))

LDLIBS       += $(external_libs)
CPPFLAGS     += $(addprefix -I,$(include_dirs))
vpath %.hpp $(include_dirs)

all:

libs_modules := source/Loader source/Solver source/Scientific
prog_modules := example/CuO example/TwistedGraphene
test_modules := test/gold test/perf test/unit

include $(addsuffix /module.mk,$(libs_modules))
include $(addsuffix /module.mk,$(prog_modules))
include $(addsuffix /module.mk,$(test_modules))
 
all: $(libraries) $(examples) $(tests)
.PHONY: all

library: $(libraries)
.PHONY: library

example: library $(examples)
.PHONY: example

test: library $(tests)
.PHONY: test

%.o: %.cpp
	@echo Compile: $<
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	@echo Clean all binaries ...
	@rm -f $(objects) $(examples) $(tests) $(libraries) 
