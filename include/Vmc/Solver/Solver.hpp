#pragma once

namespace Vmc
{
class Solver
{
public:
    Solver();
    bool solve() const;
};
}
