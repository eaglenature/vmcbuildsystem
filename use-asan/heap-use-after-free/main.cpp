// g++ -O -g -fsanitize=address main.cpp -fuse-ld=gold
int main(int argc, char** argv)
{
    int *array = new int[100];
    delete [] array;
    return array[argc];  // BOOM    
}
// $ g++ -O -g -fsanitize=address main.cpp -fuse-ld=gold
// $ ./a.out 
// =================================================================
// ==2109==ERROR: AddressSanitizer: heap-use-after-free on address 0x614000000044 at pc 0x0000004007fe bp 0x7ffcf4bb6010 sp 0x7ffcf4bb6008
// READ of size 4 at 0x614000000044 thread T0
//     #0 0x4007fd in main /home/cray/projects/use-asan/heap-use-after-free/main.cpp:6
//     #1 0x7f78d58ecf44 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21f44)
//     #2 0x4006f8  (/home/cray/projects/use-asan/heap-use-after-free/a.out+0x4006f8)

// 0x614000000044 is located 4 bytes inside of 400-byte region [0x614000000040,0x6140000001d0)
// freed by thread T0 here:
//     #0 0x7f78d5d70a90 in operator delete[](void*) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xdca90)
//     #1 0x4007c8 in main /home/cray/projects/use-asan/heap-use-after-free/main.cpp:5

// previously allocated by thread T0 here:
//     #0 0x7f78d5d6fd90 in operator new[](unsigned long) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xdbd90)
//     #1 0x4007b8 in main /home/cray/projects/use-asan/heap-use-after-free/main.cpp:4

// SUMMARY: AddressSanitizer: heap-use-after-free /home/cray/projects/use-asan/heap-use-after-free/main.cpp:6 in main
// Shadow bytes around the buggy address:
//   0x0c287fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//   0x0c287fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//   0x0c287fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//   0x0c287fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//   0x0c287fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
// =>0x0c287fff8000: fa fa fa fa fa fa fa fa[fd]fd fd fd fd fd fd fd
//   0x0c287fff8010: fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd
//   0x0c287fff8020: fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd fd
//   0x0c287fff8030: fd fd fd fd fd fd fd fd fd fd fa fa fa fa fa fa
//   0x0c287fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
//   0x0c287fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
// Shadow byte legend (one shadow byte represents 8 application bytes):
//   Addressable:           00
//   Partially addressable: 01 02 03 04 05 06 07 
//   Heap left redzone:       fa
//   Freed heap region:       fd
//   Stack left redzone:      f1
//   Stack mid redzone:       f2
//   Stack right redzone:     f3
//   Stack after return:      f5
//   Stack use after scope:   f8
//   Global redzone:          f9
//   Global init order:       f6
//   Poisoned by user:        f7
//   Container overflow:      fc
//   Array cookie:            ac
//   Intra object redzone:    bb
//   ASan internal:           fe
//   Left alloca redzone:     ca
//   Right alloca redzone:    cb
// ==2109==ABORTING
